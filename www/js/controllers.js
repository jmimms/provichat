angular.module('starter.controllers', ['emoticonizeFilter'])

.filter('unique', function() {
	return function(collection, keyname) {
		var output = [],
			keys = [];

		angular.forEach(collection, function(item) {
			var key = item[keyname];
			if(keys.indexOf(key) === -1) {
				keys.push(key);
				output.push(item);
			}
		});

		return output;
	};
})

.directive('youtube', ['$sce', function($sce) {
	return {
		restrict: 'EA',
		scope: { code:'=' },
		replace: true,
		template: '<div style="height:400px;"><iframe style="overflow:hidden;height:100%;width:100%" width="100%" height="100%" src="{{url}}" frameborder="0" allowfullscreen></iframe></div>',
		link: function (scope) {
			scope.$watch('code', function (newVal) {
				if (newVal) {
					scope.url = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + newVal);
				}
			});
		}
	};
}])

.directive('keepScroll', ['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll){
	return {
		controller : function($scope){
			var element = null;

			this.setElement = function(el){
				element = el;
			}

			this.addItem = function(item){
				if ($rootScope.waypoints && $rootScope.waypoints.waypoints && $rootScope.waypoints.position && $rootScope.waypoints.waypoints.position.up){
					element.scrollTop = (element.scrollTop+item.clientHeight+1);
				}
				else {
					$("#scrolled").scrollTop($("#scrolled")[0].scrollHeight);
				}
			};
		},
		link : function(scope,el,attr, ctrl) {
			ctrl.setElement(el[0]);
		}
	};
}])

.directive('scrollItem', function(){
	return{
		require : "^keepScroll",
		link : function(scope, el, att, scrCtrl){
			scrCtrl.addItem(el[0]);
		}
	}
})

.directive('signature', ['$rootScope', function ($rootScope){
	return {
		template: '<canvas id="canvas" height="200" style="border: 1px solid #ccc;text-align:center;"></canvas>',
		restrict: 'E',
		link: function (scope, element, attrs) {
			var canvas = document.querySelector("canvas");
			$rootScope.signatures.signaturePad = new SignaturePad(canvas, {
				penColor: "#14274d",
				maxWidth: 2
			});
		}
	};
}])

.directive('twitter', [
	function() {
		return {
			link: function(scope, element, attr) {
				setTimeout(function() {
					if (window.twttr){
						window.twttr.widgets.createShareButton(
							attr.url,
							element[0],
							function(el) {}, {
								text: attr.text,
								size: attr.size,
								count: 'none',
								hashtags: attr.hashtags,
								via: attr.via
							}
						);
					}
				});
			}
		}
	}
]);
