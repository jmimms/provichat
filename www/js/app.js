angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ionic.contrib.ui.cards', 'angularMoment', 'ui.gravatar', 'emoticonizeFilter', 'zumba.angular-waypoints', 'LocalForageModule', 'ngRoute', 'ngSanitize', 'linkify'])

.run(['$ionicPlatform', '$rootScope', '$state', '$http', '$ionicPopup', 'accountServices', '$location', '$anchorScroll', '$sce', '$ionicPopover', '$q', '$localForage', '$sanitize', 'linkify', function($ionicPlatform, $rootScope, $state, $http, $ionicPopup, accountServices, $location, $anchorScroll, $sce, $ionicPopover, $q, $localForage, $sanitize, linkify) {
  $rootScope.rippled = [ 'wss://s1.ripple.com:443', 'wss://rippled.providence.solutions:443' ];
  $rootScope.receiveName = 'OurProvidence';
  $rootScope.receiveAddress = 'rEoazhPt4VJuSs6yB5irTVitjmwsXuqwyN';
  $rootScope.rippleTxtUrl = 'https://provichat.com/ripple.txt';
  $rootScope.domain = 'provichat.com';

  $rootScope.info = {};
  $rootScope.info.backup = true;

  $rootScope.signatures = {};
  $rootScope.signatures.signatures = [];

  $rootScope.chatData = {};
  $rootScope.chatData.messages = [];

  $rootScope.loggedInAccount = {};
  $rootScope.balances = {};

  $rootScope.packet = {};

  $rootScope.pledge = {};

  var Remote = ripple.Remote;
  $rootScope.remote = new Remote({
	servers: $rootScope.rippled
  });

  $rootScope.globalRemote = new Remote({
	servers: $rootScope.rippled
  });

  $localForage.getItem('lastTransactions').then(function(data) {
  	if (!data){
		$localForage.setItem('lastTransactions', JSON.stringify([])).then(function(data) {

		});
	}
  });

  accountServices.getRootTransactions();

  $rootScope.$watch('packet.rippleName', _.debounce(function (newValue, oldValue) {
	if (newValue){
		$http.get('https://id.ripple.com/v1/user/' + newValue).
			success(function(data, status, headers, config) {
				if (data && data.address){
					$rootScope.blob = data;
				}
				else {
					$rootScope.blob = '';
				}
			}).
			error(function(data, status, headers, config) {
				$rootScope.blob = '';
			});
	}
	else {
		$rootScope.blob = '';
	}
  }, 500), true);

  try {
	$rootScope.socket = io.connect('https://invest.providence.solutions');
	accountServices.socket();
	accountServices.messageSubscribe();
  }
  catch(err) {
  }

  $rootScope.sendMessage = function(){
	if ($rootScope.chatData.message && $rootScope.loggedInAccount.publicKey){
		$rootScope.chatData.sending = true;
		var amount = ripple.Amount.from_human('0.1XRP');
		var rippleFunc = function(){
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: amount
			});

			transaction.addMemo('Provichat', JSON.stringify({type : 'chatRoom', rooms : ['provichat']}));

			transaction.addMemo('message', $rootScope.chatData.message.toString());

			transaction.submit(function(err, res) {
				$rootScope.$apply(function(){
					if (!err){
						var message = {};
						message.message = $sce.trustAsHtml($sanitize(linkify.twitterRippleProvidence(angular.copy($rootScope.chatData.message))));
						message.date = res.tx_json.date;
						message.hash = res.tx_json.hash;
						message.address = res.tx_json.Account;
						message.username = $rootScope.loggedInAccount.rippleName;
						var inRay = _.find($rootScope.chatData.messages, function(sigged) {return message.hash == sigged.hash;});
						if (!inRay){
							$rootScope.chatData.messages.unshift(message);
						}
					}
					$rootScope.chatData.message = '';
					$rootScope.chatData.sending = false;
				});
			});
		}
		if ($rootScope.remote.isConnected()){
			rippleFunc();
		}
		else {
			$rootScope.remote.connect(function() {
				rippleFunc();
			});
		}
	}
  }

	$rootScope.requestToken = function(){
		var BlobClient = new ripple.VaultClient();
		BlobClient.requestToken($rootScope.loggedInAccount.twoFactor.blob_url, $rootScope.loggedInAccount.twoFactor.blob_id, 1, function(){

		});
	}

	$rootScope.login = function(){
		var defer = $q.defer();
		var VC = new ripple.VaultClient();
		VC.loginAndUnlock($rootScope.packet.rippleName, $rootScope.packet.password, '34535345', function(err,info){
			console.log(err);
			$rootScope.loggedInAccount.connecting = false;
			$rootScope.swirl = false;
			$rootScope.$apply(function(){
				if (err && err.twofactor){
					$rootScope.loggedInAccount.twoFactor = err.twofactor;
				}
				else if (!err && info){
					$rootScope.loggedInAccount = {};
					$rootScope.loggedInAccount.rippleName = info.username;
					$rootScope.loggedInAccount.publicKey = info.blob.data.account_id;
					$rootScope.loggedInAccount.email = info.blob.data.email;
					$rootScope.loggedInAccount.secretKey = info.secret;
					$rootScope.blob.secret = info;
					$localForage.getItem('profile').then(function(data) {
						if (data){
							var datum = JSON.parse(data);
							if (datum && datum[0]){
								datum.forEach(function(dat){
									if (dat.rippleName == $rootScope.loggedInAccount.rippleName){
										$rootScope.info.name = dat.name;
										$rootScope.info.email = dat.email;
									}
								});
							}
						}
					});
					if ($rootScope.pledge && $rootScope.pledge.admins){
						$rootScope.pledge.admins.forEach(function(admin){
							if (admin.rippleAddress ==  $rootScope.loggedInAccount.publicKey){
								$rootScope.admin = admin;
							}
						});
					}
					var rippleFunc = function(){
						accountServices.getInfo();
						$rootScope.remote.requestUnsubscribe(['transactions'], function(){
							var request = $rootScope.remote.requestSubscribe(['transactions']);
							request.setServer($rootScope.rippled);

							$rootScope.remote.on('transaction', function onTransaction(transaction) {
								accountServices.accountInfo(transaction);
								accountServices.messageInfo(transaction);
							});
							request.request();
						});
					}
					if ($rootScope.remote.isConnected()){
						rippleFunc();
					}
					else {
						$rootScope.remote.connect(function() {
							rippleFunc();
						});
					}
					defer.resolve();
				}
			});
		});
		return defer.promise;
	}

	$rootScope.connectRipplePrompt = function(){
		var deferred = $q.defer();
		var myPopup = $ionicPopup.show({
			template: '<span class="providence-primary" style="position: absolute;text-align: center;left: 50%;top: 55px;margin-left: -7px;" ng-if="loggedInAccount.connecting"><i class="icon ion-loading-c" style="height: 14px;width: 13px;"></i></span><input type="text" placeholder="Ripple Name" ng-model="packet.rippleName" style="padding:5px;color: #14274d;"><i ng-if="blob" class="icon ion-checkmark-circled" style="position: absolute;top: 88px;left: 100%;margin-left: -40px;font-size: 20px;background: #fff;width: 30px;padding-left: 10px;color: #14274d;"></i><input type="password" placeholder="Password" style="padding:5px;color: #14274d;" ng-model="packet.password"><input ng-if="loggedInAccount.twoFactor" type="text" placeholder="Token" style="padding:5px;color: #14274d;" ng-model="packet.twofactor"><button ng-click="requestToken()" style="text-align:center;margin-top:10px;" ng-if="loggedInAccount.twoFactor"><span class="providence-primary smallButton marketing-primary-bg">SMS Token</span></button>',
			title: '<span class="providence-primary ionHead">Connect Ripple</span>',
			subTitle: '<a target="_blank" href="https://rippletrade.com" class="providence-primary">Have a Ripple account?</span>',
			scope: $rootScope,
			buttons: [
				{
					text: '<span class="providence-primary smallButton">Cancel</span>',
					type: 'marketing-secondary-bg'
				},
				{
					text: '<span class="providence-primary smallButton">Connect</span>',
					type: 'marketing-primary-bg',
					onTap: function(e) {
						if (!$rootScope.packet.rippleName || !$rootScope.packet.password) {
							e.preventDefault();
						}
						else {
							$rootScope.swirl = true;
							$rootScope.loggedInAccount.connecting = true;
							e.preventDefault();
							if ($rootScope.loggedInAccount.twoFactor){
								var options = {
									url         : $rootScope.loggedInAccount.twoFactor.blob_url,
									id          : $rootScope.loggedInAccount.twoFactor.blob_id,
									device_id   : $rootScope.loggedInAccount.twoFactor.device_id,
									token       : $rootScope.packet.twofactor,
									remember_me : true
								};
								new ripple.VaultClient().verifyToken(options, function(err, resp) {
									if (err) {

									} else {
										$rootScope.login().then(function(){
											myPopup.close();
											deferred.resolve();
										});
									}
								});
								$rootScope.loggedInAccount.twoFactor;
							}
							else {
								$rootScope.login().then(function(){
									myPopup.close();
									deferred.resolve();
								});
							}
						}
					}
				}
			]
		});
		return deferred.promise;
	}

  moment.locale('en', {
	relativeTime : {
		future: "IN %s",
		past:   "%s AGO",
		s:  "SECONDS",
		m:  "ONE MINUTE",
		mm: "%d MINUTES",
		h:  "ONE HOUR",
		hh: "%h HOURS",
		d:  "ONE DAY",
		dd: "%d DAYS",
		M:  "ONE MONTH",
		MM: "%d MONTHS",
		y:  "ONE YEAR",
		yy: "%d YEARS"
	}
  });

  $rootScope.getAvailable = function(){
	  var rippleFunc = function(){
		  $rootScope.remote.request('account_offers', $rootScope.receiveAddress, function(err, info) {
			  if (info && info.offers && info.offers[0]){
				  var offerTotal = 0;
				  info.offers.forEach(function(offer){
					  if (offer.taker_gets.currency == 'PVD'){
						  offerTotal += Number(offer.taker_gets.value);
					  }
				  });
				  $rootScope.sharesAvailable = offerTotal;
			  }
		  });
	  }
	  if ($rootScope.remote.isConnected()){
		  rippleFunc();
	  }
	  else {
		  $rootScope.remote.connect(function() {
			  rippleFunc();
		  });
	  }
  }

  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}])

.config(['$stateProvider', '$urlRouterProvider', '$compileProvider', function($stateProvider, $urlRouterProvider, $compileProvider) {
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|^\s*data:image\//);
  $stateProvider
    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

  $urlRouterProvider.otherwise('/');

}]);

